
#include <ArduinoLowPower.h> //doing conversion from int to hex
#include <SigFox.h> //Generic sigfox library for the mkr1200 arduino board
#include <Arduino_MKRENV.h> //Library for the mkr env shield
#include "conversions.h"

//Variable to be used in RTC wakeup loop
int alarm_source = 0; 

// Struct used to send data to thingispeak cloud
typedef struct __attribute__ ((packed)) sigfox_mesage {
  int16_t temperature;
  uint16_t humidity;
  uint16_t pressure;
  uint16_t illuminance;
  uint16_t uvIndex;
  uint8_t lastMessageStatus; //used to end message que
} SigfoxMessage;

//stub to be used in creation of struct
SigfoxMessage msg;

void setup() {
  //Found on arduino forums 
  //https://forum.arduino.cc/index.php?topic=533625.0
  LowPower.attachInterruptWakeup(RTC_ALARM_WAKEUP, alarmEvent0, CHANGE);

  //MKR shield must work and has this test lib with it
  if (!ENV.begin()) {
    //Serial.println("Failed to initialize MKR ENV shield!");
    reboot();
  }

  //Test if sigfox module is available
  if (!SigFox.begin()) {
    reboot();
  }
  
  //Send module to standby until we need to send a message
  SigFox.end();
  
}

void loop() {

  // read all the sensor values and convert them in to the struct
  //Notice there is conversion.h used
  float temperature = ENV.readTemperature();
  msg.temperature = convertoFloatToInt16(temperature, 60, -60);
  float humidity    = ENV.readHumidity();
  msg.humidity = convertoFloatToUInt16(humidity, 110);
  float pressure    = ENV.readPressure();
  msg.pressure = convertoFloatToUInt16(pressure, 200000);
  float illuminance    = ENV.readIlluminance();
  msg.illuminance = convertoFloatToUInt16(illuminance, 1000);
  float uvIndex    = ENV.readUVIndex();
  msg.uvIndex = convertoFloatToUInt16(uvIndex, 100);

  alarm_source = 1;

  // Start the module
  SigFox.begin();
  // Wait at least 30ms after first configuration (100ms before)
  delay(50);
  
  // Clears all pending interrupts
  SigFox.status();
  delay(1);

  //Data transmission begins
  SigFox.beginPacket();
  
  SigFox.write((uint8_t*)&msg, 12);

  //Final message in and transmission is over
  msg.lastMessageStatus = SigFox.endPacket();
  
  SigFox.end();

  //Sleep for 15 minutes
  LowPower.sleep(900000);

}

//Reboot used if module is not workings
void reboot() 
{
    NVIC_SystemReset();
    while (1);
}

//RTC wakeup found on arduino forums
void alarmEvent0() {
  alarm_source = 0;
}
